jQuery(function ($) {
  $(window).off('beforeunload');

  $(document).ready(function() {
    var val = document.activeElement;
    if ($(val).attr('id') != 'menu') {
      $(val).blur();
    }
    document.getElementById('menu').scrollIntoView()
  });
});
