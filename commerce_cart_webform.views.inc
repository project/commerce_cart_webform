<?php

/**
 * Implements hook_views_data_alter().
 */
function commerce_cart_webform_views_data_alter(array &$data) {
  $data['commerce_order_item']['commerce_cart_webform_submission_for_order_item'] = [
    'title' => t('Webform submission(s) for this order item'),
    'help' => t('Via Commerce Cart Webform'),
    'field' => [
      'id' => 'commerce_cart_webform_submission_for_order_item',
      'click sortable' => FALSE,
    ],
  ];
}
