<?php

namespace Drupal\commerce_cart_webform\Plugin\Commerce\InlineForm;

use Drupal\commerce\Plugin\Commerce\InlineForm\InlineFormBase;
use Drupal\commerce_cart_webform\Form\AddToCartForm;
use Drupal\commerce_cart_webform\WebformExtendingTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an inline form for the commerce cart view.
 *
 * https://zanzarra.com/blog/creating-custom-checkout-pane-drupal-commerce
 *
 * @CommerceInlineForm(
 *   id = "webform_inline_form",
 *   label = @Translation("Webform inline form"),
 * )
 */
class WebformInlineForm extends InlineFormBase {

  use WebformExtendingTrait;

  /**
   * The view object.
   *
   * @var \Drupal\views\Views
   */
  protected $view;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CouponRedemption object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // The order_id is passed via configuration to avoid serializing the
      // order, which is loaded from scratch in the submit handler to minimize
      // chances of a conflicting save.
      'webform_id' => '',
      'webform_submission_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function requiredConfiguration() {
    return ['webform_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = parent::buildInlineForm($inline_form, $form_state);

    $webform = $this->entityTypeManager->getStorage('webform')
      ->load($this->configuration['webform_id']);

    if (!$webform) {
      throw new RuntimeException('Invalid webform_id given to the inline form.');
    }

    $webform_submission = NULL;
    if ($this->configuration['webform_submission_id']) {
      /** @var \Drupal\webform\Entity\WebformSubmission $webform_submission */
      $webform_submission = $this->entityTypeManager->getStorage('webform_submission')
        ->load($this->configuration['webform_submission_id']);
    }

    $elements = $webform->getElementsDecodedAndFlattened();
    foreach ($elements as $key => $element) {
      $element = $this->convertElementForSubform($element);

      if ($webform_submission) {
        $element = $this->setDefaultValueFromSubmission($key, $element, $webform_submission);
      }

      // Add renamed element to main form :
      $inline_form[AddToCartForm::ELEMENT_PREFIX . $key] = $element;
    }

    return $this->finaliseSubform($inline_form, $webform);
  }

  public function validateInlineForm(array &$inline_form, FormStateInterface $form_state, ) {
    parent::validateInlineForm($inline_form, $form_state);
  }

  private function setDefaultValueFromSubmission(int|string $key, array $element, \Drupal\webform\Entity\WebformSubmission $webform_submission) {
    $loadedValue = $webform_submission->getElementOriginalData($key);
    if (isset($element['#type'])) {
      if ($element['#type'] === 'entity_autocomplete') {
        if (is_array($loadedValue)) {
          $loadedValue = \Drupal::entityTypeManager()
            ->getStorage($element['#target_type'])
            ->loadMultiple($loadedValue);
        }
        else {
          $loadedValue = \Drupal::entityTypeManager()
            ->getStorage($element['#target_type'])->load($loadedValue);
        }
      }
      else if ($element['#type'] === 'checkboxes') {
        $checkbox_value = [];
        foreach ($loadedValue as $value) {
          $checkbox_value[$value] = $value;
        }
        $element['#value'] = $checkbox_value;
      }
      else {
        $element['#value'] = $loadedValue;
      }
    }
    else {
      $element['#value'] = $loadedValue;
    }
    $element['#default_value'] = $loadedValue;
    return $element;
  }

}
