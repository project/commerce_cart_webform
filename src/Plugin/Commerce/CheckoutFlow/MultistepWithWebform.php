<?php

namespace Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow;

/**
 * Provides the default multistep checkout flow.
 *
 * @CommerceCheckoutFlow(
 *   id = "multistep_webform",
 *   label = "Multistep - with webform as part of checkout",
 * )
 */
class MultistepWithWebform extends CheckoutFlowWithPanesBase {

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    // Note that previous_label and next_label are not the labels
    // shown on the step itself. Instead, they are the labels shown
    // when going back to the step, or proceeding to the step.
    return [
        'login' => [
          'label' => $this->t('Login'),
          'previous_label' => $this->t('Go back'),
          'has_sidebar' => FALSE,
        ],
        'webforms' => [
          'label' => $this->t('Webforms'),
          'has_sidebar' => FALSE,
          'previous_label' => $this->t('Go back'),
        ],
        'order_information' => [
          'label' => $this->t('Order information'),
          'has_sidebar' => TRUE,
          'previous_label' => $this->t('Go back'),
        ],
        'review' => [
          'label' => $this->t('Review'),
          'next_label' => $this->t('Continue to review'),
          'previous_label' => $this->t('Go back'),
          'has_sidebar' => TRUE,
        ],
      ] + parent::getSteps();
  }

}
