<?php

namespace Drupal\commerce_cart_webform\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_cart_webform\Form\AddToCartForm;
use Drupal\commerce_cart_webform\WebformExtendingTrait;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a custom message pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_cart_webform_pane",
 *   label = @Translation("Webforms"),
 *   display_label = @Translation("Additional information based on your cart"),
 *   default_step = "order_information",
 *   wrapper_element = "fieldset",
 * )
 */
class WebformPane extends CheckoutPaneBase implements CheckoutPaneInterface {

  use WebformExtendingTrait;

  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected InlineFormManager $inlineFormManager;

  /** @var array $ext_form_states */
  private array $ext_form_states = [];

  /**
   * {@inheritdoc}
   */
  public function isVisible(): bool {
    foreach ($this->order->getItems() as $order_item) {
      $purchased_entity = $order_item->getPurchasedEntity();
      $location = $this->getVariationCartLocation($purchased_entity);
      if ($purchased_entity && $location === 'checkout') {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, InlineFormManager $inline_form_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->inlineFormManager = $inline_form_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL): CheckoutPaneBase|WebformPane|\Drupal\Core\Plugin\ContainerFactoryPluginInterface|static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_inline_form'),
    );
  }

  /**
   * @return array
   *   Array keyed by webform ID, containing quantity, form_state_id,
   *   existing_submissions
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getWebformsAndQuantity(): array {
    $webforms = [];

    // Add an inline form for each order item / quantity required
    foreach ($this->order->getItems() as $order_item) {
      $purchased_entity = $order_item->getPurchasedEntity();

      $variation_webform = $this->getVariationCartWebform($purchased_entity);
      $location = $this->getVariationCartLocation($purchased_entity);
      if (!$variation_webform || $location !== 'checkout') {
        continue;
      }

      $this->addOrderItemElementToWebform($variation_webform);

      $quantity = $this->getQuantityToProcessForOrderItem($order_item);

      $existingSubmissions = [];
      for ($quantityCount = 0; $quantityCount < $quantity; $quantityCount++) {
        $existingSubmissions[$quantityCount] = $this->getExistingSubmission($variation_webform, TRUE, $order_item->id(), $quantityCount);
      }

      $webforms[$variation_webform->id()] = [
        'quantity' => $quantity,
        'form_state_id' => $order_item->id(),
        'existing_submissions' => $existingSubmissions, // optional
      ];
    }
    return $webforms;
  }

  public function buildPaneFormStates( $pane_form, $form_state, $return_type = null) {
    $this->ext_form_states = [];
    $webforms = $this->getWebformsAndQuantity();

    foreach ($webforms as $variation_webform_id => $data) {
      $quantity = $data['quantity'];
      $form_state_id = $data['form_state_id'];

      $variation_webform = Webform::load($variation_webform_id);

      if (!$variation_webform) {
        continue;
      }

      for ($quantityCount = 0; $quantityCount < $quantity; $quantityCount++) {
        if ($return_type === 'pane_form') {
          $webform_submission = $data['existing_submissions'] ? $data['existing_submissions'][$quantityCount] : NULL;
          $inline_form = $this->inlineFormManager->createInstance('webform_inline_form', [
            'webform_id' => $variation_webform->id(),
            'webform_submission_id' => $webform_submission ? $webform_submission->id() : NULL,
          ]);

          $inline_form_id = $variation_webform->id() . '_' . $form_state_id . '_' . $quantityCount;
          $pane_form[$inline_form_id] = [
            '#parents' => array_merge($pane_form['#parents'], [$inline_form_id]),
          ];
          $pane_form[$inline_form_id] = $inline_form->buildInlineForm($pane_form[$inline_form_id], $form_state);
        } else {
          $this->ext_form_states[$form_state_id][$quantityCount] = new FormState();
        }
      }
    }

    if ($return_type === 'pane_form') {
      return $pane_form;
    } else {
      return $this->ext_form_states;
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException|\Drupal\Component\Plugin\Exception\PluginException
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form): array {
    return $this->buildPaneFormStates($pane_form, $form_state, 'pane_form');
  }

  /**
   * When we validate, we set the form state for each of our subforms, saving
   * it into ext_form_states.
   *
   * {@inheritDoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $this->ext_form_states = $this->buildPaneFormStates($pane_form, $form_state);
    $this->setUserInputIntoFormState($form_state);
    parent::validatePaneForm($pane_form, $form_state, $complete_form);

    $webforms = $this->getWebformsAndQuantity();
    foreach ($webforms as $variation_webform_id => $data) {
      $variation_webform = Webform::load($variation_webform_id);
      $is_open = $variation_webform && WebformSubmissionForm::isOpen($variation_webform);

      $quantity = $data['quantity'];
      $form_state_id = $data['form_state_id'];
      for ($quantityCount = 0; $quantityCount < $quantity; $quantityCount++) {
        $values = $this->processWebformExtendedValuesFromFormState($form_state, AddToCartForm::ELEMENT_PREFIX, $variation_webform, $form_state_id, $quantityCount);

        $values['data']['form_state_id'] = $form_state_id;

        // @todo spin into its own function
        $values['data']['order_item_id'] = $form_state_id;

        if (isset($this->ext_form_states) && $this->ext_form_states) {
          $this->ext_form_states[$form_state_id][$quantityCount]->setValues($values);

          if ($is_open === TRUE) {
            $this->ext_form_states[$form_state_id][$quantityCount]->setValidationComplete(TRUE);
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $webforms = $this->getWebformsAndQuantity();
    foreach ($webforms as $variation_webform_id => $data) {
      $variation_webform = Webform::load($variation_webform_id);
      $is_open = $variation_webform && WebformSubmissionForm::isOpen($variation_webform);

      if (!$is_open) {
        continue;
      }

      $quantity = $data['quantity'];
      $form_state_id = $data['form_state_id'];
      for ($quantityCount = 0; $quantityCount < $quantity; $quantityCount++) {
        $ext_form_state = NULL;
        if ($this->ext_form_states && isset($this->ext_form_states[$form_state_id])) {
          $ext_form_state = $this->ext_form_states[$form_state_id][$quantityCount] ?? NULL;
        }
        if ($ext_form_state && $ext_form_state->isValidationComplete()) {
          $values = $this->processWebformExtendedValuesFromFormState($form_state, AddToCartForm::ELEMENT_PREFIX, $variation_webform, $form_state_id, $quantityCount);
          if ($values) {
            $ext_form_state->setValues($values);
          }

          $form_data = array_merge(
            $ext_form_state->getValue('data'),
            [
              'form_state_id' => $form_state_id,
              'order_item_id' => $form_state_id,
            ]
          );

          $ext_form_state->setValue('data', $form_data);
          $ext_form_state->setValue('webform_id', $variation_webform->id());
        }

        $this->submitExtendedForm($pane_form, $form_state, $variation_webform, $ext_form_state);
      }
    }
  }

  function processWebformExtendedValuesFromFormState($form_state, string $prefix, WebformInterface $webform = NULL, $form_state_id = NULL, int $quantityCount = NULL) {
    $inline_form_id = $webform->id() . '_' . $form_state_id . '_' . $quantityCount;
    $values = $form_state->getValues()[$this->getPluginId()][$inline_form_id] ?? NULL;
    if ($values) {
      $extended_values = $this->getExtendedValues($values, $prefix);
      foreach ($extended_values as $key => $value) {
        $values['data'][$key] = $value;
      }
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneSummary(): array {
    $summary = [];

    $webforms = $this->getWebformsAndQuantity();
    foreach ($webforms as $variation_webform_id => $data) {
      $submissions = $data['existing_submissions'];
      if ($submissions && count($submissions) > 0) {
        foreach ($submissions as $submission) {
          if ($submission) {
            $view_builder = \Drupal::service('entity_type.manager')
              ->getViewBuilder('webform_submission');
            $build = $view_builder->view($submission);
            $summary[] = [
              '#markup' => \Drupal::service('renderer')->render($build),
            ];
          }
        }
      }
    }
    return $summary;
  }

  /**
   * @param $form_state
   *
   * @return void
   */
  public function setUserInputIntoFormState(&$form_state): void {
    $user_input = isset($form_state->getUserInput()[$this->pluginId]) ? $form_state->getUserInput()[$this->pluginId] : [];
    foreach ($user_input as $form_id => $ext_values) {
      foreach ($ext_values as $key => $input_value) {
        if (is_array($input_value)) {
          foreach ($input_value as $hold => $val) {
            if ($val) {
              $user_input[$form_id][$key][] = $val;
            }
            if (!is_numeric($hold)) {
              unset($user_input[$form_id][$key][$hold]);
            }
          }
        }
        else {
          $user_input[$form_id][$key] = $input_value;
        }
      }
    }
    $form_state->setValue($this->pluginId, $user_input);
  }

}
