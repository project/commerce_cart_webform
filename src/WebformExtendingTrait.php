<?php

namespace Drupal\commerce_cart_webform;

use Drupal\commerce_cart_webform\Form\AddToCartForm;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Utility\WebformElementHelper;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionForm;

trait WebformExtendingTrait {

  /**
   * @param $form
   * @param $form_state
   * @param \Drupal\webform\Entity\Webform $variation_webform
   * @param \Drupal\Core\Form\FormStateInterface|null $ext_form_state
   * @param int $quantityCount
   *
   * @return \Drupal\Core\Entity\ContentEntityBase|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|\Drupal\webform\Entity\WebformSubmission|null
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitExtendedForm($form, FormStateInterface $form_state, Webform $variation_webform, FormStateInterface $ext_form_state = NULL, int $quantityCount = 0): \Drupal\Core\Entity\ContentEntityBase|\Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityBase|WebformSubmission|null {
    $is_open = WebformSubmissionForm::isOpen($variation_webform);
    if (!$is_open) {
      return NULL;
    }

    $values = array_merge(
      $ext_form_state && $ext_form_state->getValues() ? $ext_form_state->getValues() : [],
      [
        'webform_id' => $variation_webform->id(),
        'in_draft' => 1,
      ]
    );

    $order_item_id = NULL;
    if (isset($values['data']) && $values['data']) {
      if (isset($values['data']['order_item_id']) && $values['data']['order_item_id']) {
        $order_item_id = $values['data']['order_item_id'];
      }
      else {
        if (isset($values['data']['form_state_id']) && $values['data']['form_state_id']) {
          $order_item_id = $values['data']['form_state_id'];
        }
      }
    }

    if ($order_item_id != NULL) {
      $order_item = OrderItem::load($order_item_id);
      if ($order_item) {
        $order_item->webform_data = serialize($form_state->getValues());
        $order_item->save();
      }
    }

    // Re-use existing submission if possible, otherwise create new.
    $webform_submission = $this->getExistingSubmission($variation_webform, TRUE, $order_item_id, $quantityCount);
    if (!$webform_submission) {
      $webform_submission = WebformSubmission::create($values);
    }
    else {
      if (isset($values['data'])) {
        foreach ($values['data'] as $key => $value) {
          $webform_submission->setElementData($key, $value);
        }
      }
    }

    $webform_submission->save();

    // Handle and log error message on submission.
    // WebformSubmissionForm::submitWebformSubmission($webform_submission);
    // if ( $webform_submission != NULL ){
    //  $order_item->cart_webform_submission = $webform_submission;
    // }
    // $order_item->save();

    return $webform_submission;
  }

  /**
   * @param $webform
   * @param $order_item
   * @param $quantityCount
   *
   * @return \Drupal\webform\Entity\WebformSubmission|null
   */
  public function getExistingSubmission(WebformInterface $webform, bool $include_draft = TRUE, int $order_item_id = NULL, int $quantityCount = NULL): ?WebformSubmission {
    if (!$order_item_id) {
      return NULL;
    }

    try {
      $order_item = OrderItem::load($order_item_id);
      $submissions = $this->getSubmissionsForOrderItem($order_item);
      $submissions_values = array_values($submissions);
      if (count($submissions_values) == 0) {
        return NULL;
      }
      return $submissions_values[$quantityCount] ?: NULL;
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    catch (PluginNotFoundException $e) {
    }
  }

  /**
   * Create a private, hidden element to link the submission to the order item
   *
   * @param \Drupal\webform\WebformInterface $variation_webform
   *
   * @return \Drupal\webform\WebformInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  function addOrderItemElementToWebform(\Drupal\webform\WebformInterface $variation_webform): \Drupal\webform\WebformInterface {
    $elements = $variation_webform->getElementsDecoded();

    if (!isset($elements['order_item_id'])) {
      $variation_webform->setElements(array_merge(
        $elements,
        [
          'order_item_id' => [
            '#type' => 'entity_autocomplete',
            '#target_type' => 'commerce_order_item',
            '#selection_handler' => 'default:default',
            '#selection_settings' => [
              'include_anonymous' => FALSE,
            ],
            '#title' => 'Order item',
            '#value' => NULL,
            '#hidden' => TRUE,
            '#private' => TRUE,
            '#wrapper_attributes' => ['class' => ['hidden']],
          ],
        ]
      ));
      $variation_webform->save();
    }
    return $variation_webform;
  }

  function processWebformExtendedValuesFromFormState($form_state, string $prefix, WebformInterface $webform = NULL, OrderItemInterface $orderItem = NULL, int $quantityCount = NULL): array {
    $values = [];

    $extended_values = $this->getExtendedValues($form_state->getValues(), $prefix);
    foreach ($extended_values as $key => $value) {
      $values['data'][$key] = $value;
    }
    return $values;
  }

  function getExtendedValues($values, string $prefix): array {
    $extended_values = [];
    $cleaned_values = [];
    foreach ($values as $key => $value) {
      if (strpos($key, $prefix) !== FALSE) {
        $extended_values[$key] = $value;
      }
    }

    foreach ($extended_values as $key => $value) {
      $new_key = str_replace($prefix, '', $key);
      $cleaned_values[$new_key] = $value;
    }

    return $cleaned_values;
  }

  /**
   * @param array $form
   *
   * @return \Drupal\commerce_order\Entity\OrderItemInterface|null
   */
  public function getOrderItem(array $form): ?\Drupal\commerce_order\Entity\OrderItemInterface {
    $order_item = NULL;

    if ($this->entity && $this->entity->id() != NULL) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->entity;
    }
    else {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $form['order_item'] ?: NULL;
    }
    return $order_item;
  }

  public function getQuantityToProcessForOrderItem($order_item) {
    $purchased_entity = $order_item->getPurchasedEntity();
    if ($purchased_entity->cart_webform_form_frequency->value == 'every_quantity') {
      return $order_item->getQuantity();
    }
    else {
      return 1;
    }
  }

  public function finaliseSubform($form, $webform) {
    // Process tokens
    $tokenManager = \Drupal::service('webform.token_manager');
    $form = $tokenManager->replace($form, $webform);

    // Attach libraries for webform
    $this->attachWebformLibraries($form);

    return $form;
  }

  public function attachWebformLibraries(&$form): void {
    // Attach the webform library.
    $form['#attached']['library'][] = 'webform/webform.form';

    // Autofocus: Save details open/close state.
    $form['#attributes']['class'][] = 'js-webform-autofocus';
    $form['#attached']['library'][] = 'webform/webform.form.auto_focus';

    // Unsaved: Warn users about unsaved changes.
    $form['#attributes']['class'][] = 'js-webform-unsaved';
    $form['#attached']['library'][] = 'webform/webform.form.unsaved';

    // Details save: Attach details element save open/close library.
    $form['#attached']['library'][] = 'webform/webform.element.details.save';

    // Details toggle: Display collapse/expand all details link.
    $form['#attributes']['class'][] = 'js-webform-details-toggle';
    $form['#attributes']['class'][] = 'webform-details-toggle';
    $form['#attached']['library'][] = 'webform/webform.element.details.toggle';
  }

  /**
   * @return bool
   */
  private function checkVariationCartSelection($variation): bool {
    if ($variation->cart_webform_location->value == 'cart') {
      return TRUE;
    }
    $product = $variation->getProduct();
    if ($product && $product->cart_webform_location->value == 'cart') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @param $variation
   *
   * @return \Drupal\webform\WebformInterface|null
   */
  private function getVariationCartWebform($variation): ?WebformInterface {
    if (!$variation) {
      return NULL;
    }

    $fieldWebform = $variation->cart_webform;
    if (is_array($fieldWebform)) {
      $webform = NULL;
    }
    else {
      $webform = $fieldWebform ? $fieldWebform->entity : NULL;
    }

    if (!$webform) {
      $product = $variation->getProduct();
      if ($product) {
        $fieldWebform = $product->cart_webform;
        if (is_array($fieldWebform)) {
          $webform = NULL;
        }
        else {
          $webform = $fieldWebform ? $fieldWebform->entity : NULL;
        }
      }
    }

    if ($webform && !$webform->isOpen()) {
      return NULL;
    }

    return $webform;
  }

  /**
   * @param $variation
   *
   * @return bool|null
   */
  private function getVariationCartLocation($variation): ?string {
    if (!$variation) {
      return NULL;
    }

    $cartWebformLocation = $variation->cart_webform_location;
    if ($cartWebformLocation && !is_array($cartWebformLocation) && $variation->cart_webform_location->value) {
      return $variation->cart_webform_location->value;
    }

    $product = $variation->getProduct();
    $cartWebformLocation = $product ? $product->cart_webform_location : NULL;
    if ($cartWebformLocation && !is_array($cartWebformLocation) && $product->cart_webform_location->value) {
      return $product->cart_webform_location->value;
    }

    return NULL;
  }

  private function convertElementForSubform(array $element): array {
    // Transfer states to the new elements with renaming,
    // so it picks up the renamed element:
    if (isset($element['#states'])) {
      foreach ($element['#states'] as $type => $state_values) {
        foreach ($state_values as $id => $conditions) {
          $new_id = str_replace('name="', 'name="' . AddToCartForm::ELEMENT_PREFIX, $id);
          $element['#states'][$type][$new_id] = $conditions;
          $required = $element['#required'] ?? FALSE;
          if (!isset($element['#states']['required']) && isset($element['#states']['visible'])) {
            if ($required) {
              $element['#states']['required'][$new_id] = ['value' => 1];
            }
          }
          if (!isset($element['#states']['required']) && isset($element['#states']['hidden'])) {
            $element['#states']['required'][$new_id] = ['value' => 0];
          }
          unset($element['#states'][$type][$id]);
        }
      }
    }

    // Process All Webform Elements
    WebformElementHelper::process($element);

    return $element;
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getSubmissionsForOrderItem(OrderItemInterface $item, $include_draft = TRUE): array {
    $submission_storage = \Drupal::entityTypeManager()
      ->getStorage('webform_submission');

    $variation = $item->getPurchasedEntity();

    $query = \Drupal::database()->select('webform_submission_data', 'wsd')
      ->fields('wsd', ['sid'])
      ->condition('name', 'order_item_id')
      ->condition('value', $item->id())
      ->distinct();

    // We don't REQUIRE a webform because sometimes the pane will be extending
    // WebformPane, rather than being pulled from a product.
    $webform = $this->getVariationCartWebform($variation);
    if ($webform) {
      $query->condition('webform_id', $webform->id());
    }

    if (!$include_draft) {
      $query->join('webform_submission', 'ws', 'ws.sid = wsd.sid');
      $query->condition('in_draft', FALSE);
    }

    $ids = $query->execute()->fetchCol();

    return $submission_storage->loadMultiple($ids);
  }

}
