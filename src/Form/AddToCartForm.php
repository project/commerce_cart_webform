<?php

namespace Drupal\commerce_cart_webform\Form;

use Drupal;
use Drupal\commerce_cart\Form\AddToCartForm as FormAddToCartForm;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Entity\Webform;

/**
 * Provides the order item add to cart form.
 *
 * https://docs.drupalcommerce.org/commerce2/developer-guide/products/displaying-products/code-recipes
 */
class AddToCartForm extends FormAddToCartForm {

  use Drupal\commerce_cart_webform\WebformExtendingTrait;

  /**
   * Used to prefix elements when added to other form.
   */
  public const ELEMENT_PREFIX = 'ccwex_';

  /**
   * @var Webform
   */
  private ?Webform $variation_webform = NULL;

  /**
   * @var \Drupal\commerce\PurchasableEntityInterface|null
   */
  public ?Drupal\commerce\PurchasableEntityInterface $selected_variation = NULL;

  /** @var FormStateInterface $ext_form_state */
  private ?FormStateInterface $ext_form_state = NULL;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\commerce_product\Entity\Product $product */
    $product = $form_state->get('product');

    /** @var \Drupal\commerce_product\Entity\ProductVariation $selected_variation_id */
    $selected_variation_id = $form_state->get('selected_variation');
    if (!empty($selected_variation_id)) {
      /** @var \Drupal\commerce_product\Entity\ProductVariation $selected_variation */
      $this->selected_variation = ProductVariation::load($selected_variation_id);
    }
    else {
      /** @var \Drupal\commerce_product\Entity\ProductVariation $selected_variation */
      $this->selected_variation = $product->getDefaultVariation();
    }

    if ($this->checkVariationCartSelection($this->selected_variation)) {
      // Require a webform.
      $variation_webform = $this->getVariationCartWebform($this->selected_variation);
    }
    else {
      // No webform applicable.
      return $form;
    }

    if (!$variation_webform || !$variation_webform instanceof Webform) {
      return $form;
    }

    $this->variation_webform = $variation_webform;

    $form_state->set('variation_webform', $variation_webform->id());

    $this->ext_form_state = new FormState();

    // Get webform elements:
    $elements = $variation_webform->getElementsInitializedAndFlattened();
    $this->addOrderItemElementToWebform($this->variation_webform);

    // Copy elements to add to cart form:
    foreach ($elements as $key => $element) {
      $form[AddToCartForm::ELEMENT_PREFIX . $key] = $this->convertElementForSubform($element);
    }

    $form = $this->finaliseSubform($form, $this->variation_webform);

    // Add an order item element to capture order item.
    $form['order_item'] = NULL;

    $form['#webform_id'] = $this->variation_webform->id();

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->checkVariationCartSelection($this->selected_variation)) {
      $entity = parent::validateForm($form, $form_state);

      // @todo move out of here; this is really an issue with other modules.
      $this->fixQuantityError($form_state);

      return $entity;
    }

    if ($this->variation_webform == NULL) {
      $purchased_entity_id = $form_state->getValue('purchased_entity')[0]['variation'];
      $variation = ProductVariation::load($purchased_entity_id);
      $variation_webform = $this->getVariationCartWebform($variation);
      $this->variation_webform = $variation_webform;
    }

    if ($this->ext_form_state == NULL) {
      $this->ext_form_state = new FormState();
    }

    parent::validateForm($form, $form_state);

    // @todo move out of here; this is really an issue with other modules.
    $this->fixQuantityError($form_state);

    if ($this->variation_webform && $this->ext_form_state) {
      $values = $this->processWebformExtendedValuesFromFormState($form_state, AddToCartForm::ELEMENT_PREFIX);

      if (count($values) > 0) {
        $order_item = $this->getOrderItem($form);
        if ($order_item) {
          $values['data']['form_state_id'] = $order_item->id();
          $values['data']['order_item_id'] = $order_item->id();
        }

        $this->ext_form_state->setValues($values);
      }
    }
  }

  /**
   * {@inheritDoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $commerce_cart_skip = FALSE;
    if (Drupal::moduleHandler()
        ->moduleExists('commerce_cart_skip') && $form_state->get('commerce_cart_skip_submit')) {
      $commerce_cart_skip = TRUE;
      commerce_cart_skip_submit($form, $form_state);
    }

    $submitParent = !$commerce_cart_skip;
    if ($submitParent) {
      parent::submitForm($form, $form_state);
    }

    if ($this->variation_webform == NULL) {
      $purchased_entity_id = $form_state->getValue('purchased_entity')[0]['variation'];
      $variation = ProductVariation::load($purchased_entity_id);
      $variation_webform = $this->getVariationCartWebform($variation);
      $this->variation_webform = $variation_webform;
    }

    if ($this->variation_webform) {
      $order_item = $this->getOrderItem($form);

      if ($order_item != NULL && $this->variation_webform != NULL && $this->ext_form_state != NULL) {
        $form_data = array_merge(
          $this->ext_form_state->getValue('data'),
          [
            'order_item_id' => $order_item->id(),
            'form_state_id' => $order_item->id(),
          ]
        );

        $this->ext_form_state->setValue('data', $form_data);
        $this->ext_form_state->setValue('webform_id', $this->variation_webform->id());
      }

      if ($this->checkVariationCartSelection($this->selected_variation)) {
        $this->submitExtendedForm($form, $form_state, $this->variation_webform, $this->ext_form_state);
      }
    }
  }

  private function fixQuantityError(&$form_state) {
    if (isset($form_state->getErrors()['purchased_entity']) && $purchasedEntityError = $form_state->getErrors()['purchased_entity']) {
      if (count($form_state->getErrors()) == 1) {
        $form_state->clearErrors();
      }
      $form_state->setErrorByName('quantity', $purchasedEntityError);
    }
  }

}
